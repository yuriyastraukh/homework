
from django.views.generic import TemplateView, UpdateView, ListView
from django.http import HttpResponseRedirect
from admincore.views import HeadBar
from products.models import Books
from cart.models import Products, Cart

class Homepage(TemplateView):
    template_name = 'shoptemplate.html'
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = 'BookStore'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['new_list'] = Books.boo.filter(date_add__year=2018, act_on_off=True).order_by('-rate_b')[0:5]
        context['p_list'] = Books.boo.filter(rate_b__gt=7, act_on_off=True).order_by('-rate_b', "name")[0:12]
        return context

class UserView(TemplateView):
    HttpResponseRedirect('shop-user/purchases/')


class AboutUs(TemplateView):
    template_name = 'template_static.html'
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = 'О нас'
        context['dlist'] = HeadBar.create_bar(self.request)
        return super().get_context_data(**context)




class Delivery(TemplateView):
    template_name = 'template_static.html'
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = 'Доставка'
        context['dlist'] = HeadBar.create_bar(self.request)
        return super().get_context_data(**context)

class Purchases(ListView):
    template_name = 'shoptemplate_store.html'
    model = Books
    paginate_by = 12
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = 'Магазин'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['p_list'] = Books.boo.all()[90:100]
        return super().get_context_data(**context)

class PurchasesAdd(UpdateView):
    template_name = 'shoptemplate.html'
    model = Products
    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)
    sucsess_url="shop-user/purchases/"

