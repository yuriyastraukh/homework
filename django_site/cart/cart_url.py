from cart.views import AddProductToCart, CartList, DeleteBookFromCart, ChangeBookInCart
from django.urls import path

app_name = 'cart'

urlpatterns = [
    path('addtocart/<int:pk>', AddProductToCart.as_view(), name='book_add'),
    path('list/', CartList.as_view(), name='cart_list'),
    path('delete/<int:pk>', DeleteBookFromCart.as_view(), name='delete_book_from_cart'),
    path('changebook/<int:pk>', ChangeBookInCart.as_view(), name='change_book_in_cart'),
]