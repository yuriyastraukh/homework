from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from products.models import Books
from admincore.views import HeadBar
from django.db.models import Q
from django.core.paginator import Paginator

class Makesearch(TemplateView):
    template_name = 'results_of_search.html'
    paginate_by = 10
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Результаты поиска'
        context['dlist'] = HeadBar.create_bar(self.request)
        query_f = self.request.GET.get("query")
        context['results'] = Books.boo.filter(Q(name__icontains=query_f) | Q(author_f__name__icontains=query_f) | Q(year_p__icontains=query_f))
        return context