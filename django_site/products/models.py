from django.db import models
from reference.models import Authors, Series, Genre, Publisher, Producer
from django.core.validators import MaxValueValidator, MinValueValidator, ValidationError
import re
from django.forms import CharField
from django import forms

def my_validator(val):
    print(val)
    if re.search('\D', val):
        raise ValidationError('ISBN contains illegal value')
    if len(val) != 13:
        raise ValidationError('ISBN length is unfit')

class Books(models.Model):
    name = models.CharField(verbose_name='Название книги', max_length=250)
    image_f = models.ImageField(verbose_name='Изображение', upload_to="images/")
    price = models.DecimalField(verbose_name="Цена", max_digits=9, decimal_places=2,
                                validators=[MinValueValidator(0.01)])
    author_f = models.ManyToManyField(Authors, verbose_name='Автор', null=True, blank=True)
    seriesOfbook = models.ManyToManyField(Series, verbose_name='Серия', max_length=250, null=True, blank=True)
    genre = models.ManyToManyField(Genre, verbose_name='Жанр', max_length=250,null=True, blank=True)
    year_p = models.IntegerField(verbose_name='Год издания', validators=[MinValueValidator(1995)])
    nompages = models.IntegerField(verbose_name='Количестиво страниц')
    binding_type = (
        ('мягкий', 'Мягкий'),
        ('твердый', 'Твердый'),
        ('суперобложка', 'Суперобложка'),
    )
    binding = models.CharField(verbose_name='Переплет', choices=binding_type, max_length=250, null=True, blank=True)
    formatofbook = models.CharField(verbose_name='Формат', max_length=250, blank=True)
    ISBN = models.CharField(verbose_name='ISBN', max_length=250, validators=[my_validator])
    weight = models.DecimalField(verbose_name='Вес', max_digits=5, decimal_places=2)
    age_type = (('+0', '+0'),('16+', '16+'),('18+', '18+'),)
    age_rest = models.CharField(verbose_name='Возрастные ограничения', choices=age_type, max_length=250, null=True, blank=True)
    publisher = models.ForeignKey(Publisher, verbose_name='Издательство', on_delete=True, null=True, blank=True)
    producer = models.ForeignKey(Producer, verbose_name='Производитель', on_delete=True, null=True, blank=True)
    stockofbooks = models.PositiveIntegerField(verbose_name='Количество книг в наличии')
    act_on_off = models.BooleanField(verbose_name='Доступно для заказа', null=True)
    rate_b = models.IntegerField(verbose_name='Рейтинг',
                                 validators=[
                                     MinValueValidator(1),
                                     MaxValueValidator(10)]
                                 )
    date_add = models.DateField(verbose_name='Дата внесения в каталог', auto_now_add=True)
    date_change = models.DateField(verbose_name='Дата изменения', auto_now=True)
    # date_add = models.DateField(verbose_name='Дата внесения в каталог')
    # date_change = models.DateField(verbose_name='Дата изменения')
    boo = models.Manager()
    def __str__(self):
        return  self.name



