from django.db import models
# from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
User = get_user_model()

class UserProfile(models.Model):
    user_name = models.OneToOneField(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    user_image = models.ImageField(verbose_name='Аватар_пользователя', upload_to="images/", blank=True)
    phone_number = models.TextField(verbose_name='Номер_телефона', default = '+375', blank=True)
    country_address = models.TextField(verbose_name='Cтрана', blank=True)
    city_address = models.TextField(verbose_name='Город', blank=True)
    index_address = models.TextField(verbose_name='Индекс', blank=True, null=True)
    adress1_address = models.TextField(verbose_name='Адрес1', blank=True)
    adress2_address = models.TextField(verbose_name='Адрес2', blank=True)

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'