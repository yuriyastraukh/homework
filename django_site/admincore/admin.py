from django.apps import AppConfig
from django.contrib import admin
from . import models

# class CartConfig(AppConfig):
#     name = 'cart'

class ProfileAdmin(admin.ModelAdmin):
    list_display=[
        'pk',
        'user_name',
        'phone_number',
        'adress1_address',
        'adress2_address',
    ]

    class Meta:
        model = models.UserProfile

admin.site.register(models.UserProfile, ProfileAdmin)
