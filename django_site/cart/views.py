from django.views.generic import UpdateView, DetailView, DeleteView, ListView
from products.models import Books
from admincore.views import HeadBar
from .models import Products, Cart
from .forms import AddToCartForm, ChangeBookInCart
from orders.forms import CheckoutOrderForm
from orders.models import OrderStatus



class AddProductToCart(UpdateView):
    model = Products
    template_name = 'add-to-cart.html'
    form_class = AddToCartForm

    def get_object(self):
        print(self.request.user)
        cart_id = self.request.session.get('cart_id')
        product_id = self.kwargs.get('pk')
        if not self.request.user.id == None:
            cart, cart_created = Cart.objects.get_or_create(
                pk=cart_id,
            defaults={
                'user': self.request.user
            }
            )
        else:
            cart, cart_created = Cart.objects.get_or_create(
                pk=cart_id,
            )
        book_ = Books.boo.get(pk=product_id)
        product_in_cart, created = Products.objects.get_or_create(
            cart=cart,
            book=book_,
            defaults={
                'cart': cart,
                'book': book_,
                'quantity': 1},
        )
        if cart_created:
            self.request.session['cart_id'] = cart.pk
        elif not created:
            product_in_cart.quantity += 1
        return product_in_cart

    success_url = "/"

new_order_status = OrderStatus.objects.get(pk=1)
class CartList(DetailView):
    model = Cart
    template_name = 'cart-list.html'
    def get_object(self):
        cart_id = self.request.session.get('cart_id')
        cart, cart_created = Cart.objects.get_or_create(
            pk=cart_id,
            # defaults={
            #     'user': self.request.user
            # }
        )
        return cart

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        checkot_form = CheckoutOrderForm()
        checkot_form.fields['cart'].initial = self.object
        checkot_form.fields['status'].initial = new_order_status
        context["form"] = checkot_form
        try:
            context['title'] = Cart.objects.get(pk=self.request.session.get('cart_id'))
        except Exception:
            context['title'] = "Корзина пуста"
        context['dlist'] = HeadBar.create_bar(self.request)
        return context



class DeleteBookFromCart(DeleteView):
    model = Products
    template_name = 'delete_from_cart.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Редактирование заказа/удаление товара'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/cart/list/"

class ChangeBookInCart(UpdateView):
    model = Products
    template_name = 'change_book_in_cart.html'
    form_class = ChangeBookInCart
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Редактирование заказа/изменение книги'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/cart/list/"