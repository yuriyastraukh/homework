from django.contrib import admin
from django.urls import path, include
from core.views import Homepage, UserView, AboutUs, Delivery
from search.views import Makesearch
from django.conf.urls.static import static
from django.conf import settings
from django.views.generic.base import RedirectView

urlpatterns = [
    path(r'', Homepage.as_view(), name='home'),
    path('s-admin/', admin.site.urls),
    path('shop-user/', include('core.core_url'), name='shopuser'),
    path('cart/', include('cart.cart_url'), name="cart"),
    path('order/', include('orders.orders_url'), name='orders'),
    path('shop-admin/', include('admincore.admin_url'), name='shopadmin'),
    path('books/', include('products.products_url'), name='products'),
    path('accounts/', include('admincore.accounts_url'), name='accounts'),
    path('comments/', include('comment.comments_url'), name='comments'),
    path('reference/', include('reference.reference_url'), name='reference'),
    path('search/', Makesearch.as_view(), name='makesearch'),
    path('delivery/', Delivery.as_view(), name='delivery'),
    path('about/', AboutUs.as_view(), name='about_company'),
    path('favicon.ico', RedirectView.as_view(url='/images/favicon.ico', permanent=True)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

