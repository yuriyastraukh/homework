from django.views.generic import TemplateView, RedirectView, ListView, DeleteView
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from products.models import Books
from products.models import Genre, Series
from datetime import date, timedelta
from django.urls import reverse_lazy
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import PasswordResetView, PasswordResetConfirmView, PasswordResetDoneView, PasswordResetCompleteView
from django.contrib.auth.models import User, Group
from cart.models import Cart, Products
from orders.models import Order
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from admincore.forms import SignUpForm
from admincore.models import UserProfile
from django.utils.datastructures import MultiValueDictKeyError

class HeadBar:
    def create_bar(request):
        man = False
        for i_g in request.user.groups.all():
            if i_g == Group.objects.filter(name="Managers")[0]:
                man = True
                break
        if man:
            listdict = {'Книги': {
                    'it1': {'Список книг': 'products:list_of_books'},
                    'it2': {'Добавить книгу': 'products:create_book'}, },
                'Авторы': {
                    'it1': {'Список авторов': 'reference:ShowAuthList'},
                    'it2': {'Добавить автора': 'reference:showauthorform'}, },
                'Серии': {
                    'it1': {'Список серий': 'reference:ShowSeriesList'},
                    'it2': {'Добавить серию': 'reference:ShowSeriesForm'}, },
                'Жанры': {
                    'it1': {'Список жанров': 'reference:ShowGenreList'},
                    'it2': {'Добавить жанр': 'reference:ShowGenreForm'}, },
                'Издатели': {
                    'it1': {'Список издателей': 'reference:ShowPublisherList'},
                    'it2': {'Добавить издателя': 'reference:ShowPublisherForm'}, },
                'Производители': {
                    'it1': {'Список производителей': 'reference:ShowProducerList'},
                    'it2': {'Добавить производителя': 'reference:ShowProducerForm'}, },
                'Статистика': {
                    'it1': {'Статистика сайта': 'shopadmin:admin_statistics'}, },
                'Управление пользователями': {
                    'it1': {'Список пользователей': 'shopadmin:user-list'}, },
                'Магазин': {
                    'it1': {'Купить книгу': 'shopuser:book_purchases'}, },
                'Заказы': {
                    'it1': {'Список заказов': 'orders:order-list'}, },
                # 'Доставка': {
                #     'it1': {'Доставка товаров': 'delivery'}, },
                # 'О нас': {
                #     'it1': {'О нас': 'about_company'}, },
            }
        else:
            listdict = {
                'Каталог': {
                    'it1': {'Купить книгу': 'shopuser:book_purchases'}, },
                # 'Доставка': {
                #     'it1': {'Доставка товаров': 'delivery'}, },
                # 'О нас': {
                #     'it1': {'О нас': 'about_company'}, },
            }

        if request.user.username == "admin":
            listdict.update({'Административная страница': {'it1': {'Админка': 'shopadmin:shop_admin'}, }})

        return listdict

class Statistics:
    def info():
        dist_stst = []
        dist_stst.append({'Количество книг в каталоге': Books.boo.all().count()})
        dist_stst.append({'Количество активных книг в каталоге': Books.boo.filter(act_on_off=True).count()})
        dist_stst.append({'Добавлено книг вчера': Books.boo.filter(date_add__exact=(date.today() - timedelta(1))).count()})
        dist_stst.append({'Добавлено книг за месяц': Books.boo.filter(date_add__month=12).count()})
        dist_stst.append({'Количество записей справочника \'Жанр\'': Genre.gee.all().count()})
        dist_stst.append({'Количество записей справочника \'Серии\'': Series.see.all().count()})
        return dist_stst
    def series_info():
        dict_series = []
        series_collectin = Series.see.all()
        for ex_coll in series_collectin:
            if Books.boo.filter(seriesOfbook=ex_coll).count()>0:
                dict_series.append({ex_coll: Books.boo.filter(seriesOfbook=ex_coll).count()})
        return dict_series
    def genre_info():
        dict_genre = []
        series_collectin = Genre.gee.all()
        for ex_coll in series_collectin:
            if Books.boo.filter(genre=ex_coll).count()>0:
                dict_genre.append({ex_coll: Books.boo.filter(genre=ex_coll).count()})
        return dict_genre




class AdminView(TemplateView):
     template_name = 'admintemplate.html'

     def get_context_data(self, **kwargs):
          context = {}
          context['title_'] = 'Административная страница'
          context['dlist'] = HeadBar.create_bar(self.request)
          context.update(kwargs)
          return super().get_context_data(**context)


class AdminStatistics(TemplateView):
    template_name = 'template_statistics.html'
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = 'Статистика каталога'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['statistics'] = Statistics.info()
        context['series_'] = Statistics.series_info()
        context['genre_'] = Statistics.genre_info()
        context.update(kwargs)
        return super().get_context_data(**context)


class UserInformation(TemplateView):
    template_name = 'user-information-template.html'
    model = User

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['u_inf'] = self.request.user
        orders_ = {}
        for cart_o in Cart.objects.filter(user=self.request.user):
            dict_={Order.objects.filter(cart=cart_o):Products.objects.filter(cart=cart_o)}
            orders_.update(dict_)
        context['add_inf'] = UserProfile.objects.get_or_create(user_name=self.request.user)
        context['ui'] = UserProfile.objects.get_or_create(user_name=self.request.user)[0].user_image
        print(context['ui'])
        context['title_'] = 'Информация о пользователе'
        context['orders'] = orders_
        context['dlist'] = HeadBar.create_bar(self.request)
        return context

class LogInUser(LoginView):
    template_name = 'login.html'

class LogOutUser(LogoutView):
    template_name = 'log_out.html'

class ChangePassword(PasswordChangeView):
    template_name = 'change-password.html'
    success_url = '/'


class ResetPassword(PasswordResetView):
    template_name = 'registration/password-reset.html'
    subject_template_name = 'registration/subject-reset-password.txt'
    email_template_name = 'registration/reset-email-content.html'
    from_email = 'yuriy.astraukh@yandex.ru'
    token_generator = default_token_generator
    success_url = reverse_lazy('shopadmin:password-reset-done')


class ResetPasswordDone(PasswordResetDoneView):
    template_name = 'registration/reset-password-done.html'

class ResetPasswordConfirm(PasswordResetConfirmView):
    template_name = 'registration/password-reset-confirm.html'
    token_generator = default_token_generator
    success_url = reverse_lazy('shopadmin:password-reset-complete')


class ResetPasswordComplete(PasswordResetCompleteView):
    template_name = 'registration/password-reset-complete.html'
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = 'Статистика каталога'

######################################################################
#Sign_up_view                                                        #
######################################################################



def signup(request):

    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            print(request.user)
            cu = User.objects.get(username=request.user)
            cu.groups.set(Group.objects.filter(name="Consumers"))
            cu.save()
            return redirect('/')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})
########################################################################

class UpdateUserData(TemplateView):
    template_name = 'user_data_change.html'
    def get_context_data(self, **kwargs):
        user_c = User.objects.get(username=self.request.user.username)
        user_add, user_created = UserProfile.objects.get_or_create(
            user_name=self.request.user,
        )
        return {
            'username': user_c,
            'first_name': user_c.first_name,
            'last_name': user_c.last_name,
            'email': user_c.email,
            'phone_number': user_add.phone_number,
            'country_address': user_add.country_address,
            'city_address': user_add.city_address,
            'index_address': user_add.index_address,
            'adress1_address': user_add.adress1_address,
            'adress2_address': user_add.adress2_address,
        }


class SaveUserInformation(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        user_f = self.request.user
        username_ = self.request.POST.get('username')
        first_name_ = self.request.POST.get('first_name')
        last_name_ = self.request.POST.get('last_name')
        email_ = self.request.POST.get('email')
        try:
            user_image_ = self.request.FILES['avatar']
        except MultiValueDictKeyError:
            user_image_ = None
        phone_number_ = self.request.POST.get('phone_number')
        country_address_ = self.request.POST.get('country_address')
        city_address_ = self.request.POST.get('city_address')
        index_address_ = self.request.POST.get('index_address')
        adress1_address_ = self.request.POST.get('adress1_address')
        adress2_address_ = self.request.POST.get('adress2_address')
        next = '/shop-admin/user-informaion/'

        user_model = User.objects.get(username=user_f)
        user_model.username = username_
        user_model.first_name = first_name_
        user_model.last_name = last_name_
        user_model.email = email_
        user_model.save()

        user_aditional = UserProfile.objects.get(user_name=user_f)
        user_aditional.user_name = user_f
        if user_image_ != None:
            user_aditional.user_image = user_image_
        user_aditional.phone_number = phone_number_
        user_aditional.country_address = country_address_
        user_aditional.city_address = city_address_
        user_aditional.index_address = index_address_
        user_aditional.adress1_address = adress1_address_
        user_aditional.adress2_address = adress2_address_
        user_aditional.save()
        return next

###################################################################################
class ListOfUsers(ListView):
    template_name = 'user-list.html'
    model = User

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список пользователей'
        context['dlist'] = HeadBar.create_bar(self.request)
        user_model = User.objects.select_related('userprofile').all()
        return context

class DeleteUser(DeleteView):
    template_name = 'user_delete.html'
    model = User
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удалить пользователя'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = 'user-list'

class UpdateUserDataManager(TemplateView):
    template_name = 'user_data_change_.html'
    def get_context_data(self, **kwargs):
        user_c = User.objects.get(pk=self.kwargs['pk'])
        user_add, user_created = UserProfile.objects.get_or_create(
            user_name=user_c,
        )
        return {
            'username': user_c,
            'first_name': user_c.first_name,
            'last_name': user_c.last_name,
            'email': user_c.email,
            'phone_number': user_add.phone_number,
            'country_address': user_add.country_address,
            'city_address': user_add.city_address,
            'index_address': user_add.index_address,
            'adress1_address': user_add.adress1_address,
            'adress2_address': user_add.adress2_address,
        }

class SaveUserInformationByManager(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        # user_f = self.request.user
        username_ = self.request.POST.get('username')
        first_name_ = self.request.POST.get('first_name')
        last_name_ = self.request.POST.get('last_name')
        email_ = self.request.POST.get('email')
        try:
            user_image_ = self.request.FILES['avatar']
        except MultiValueDictKeyError:
            user_image_ = None
        phone_number_ = self.request.POST.get('phone_number')
        country_address_ = self.request.POST.get('country_address')
        city_address_ = self.request.POST.get('city_address')
        index_address_ = self.request.POST.get('index_address')
        adress1_address_ = self.request.POST.get('adress1_address')
        adress2_address_ = self.request.POST.get('adress2_address')
        next = '/shop-admin/user-list/'

        user_model = User.objects.get(username=username_)
        user_model.username = username_
        user_model.first_name = first_name_
        user_model.last_name = last_name_
        user_model.email = email_
        user_model.save()

        user_aditional = UserProfile.objects.get(user_name=user_model)

        user_aditional.user_name = user_model
        if user_image_ != None:
            user_aditional.user_image = user_image_
        user_aditional.phone_number = phone_number_
        user_aditional.country_address = country_address_
        user_aditional.city_address = city_address_
        user_aditional.index_address = index_address_
        user_aditional.adress1_address = adress1_address_
        user_aditional.adress2_address = adress2_address_
        user_aditional.save()
        return next




