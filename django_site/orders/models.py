from django.db import models
from cart.models import Cart
from reference.models import OrderStatus


class Order(models.Model):
    cart = models.ForeignKey(
        Cart,
        verbose_name="Корзина",
        on_delete=models.PROTECT)
    status = models.ForeignKey(
        OrderStatus,
        verbose_name="Статус заказа",
        blank=True,
        null=True,
        on_delete=models.PROTECT)
    phone = models.CharField(
        verbose_name="Контактный телефон",
        max_length=16)
    email = models.EmailField(
        verbose_name="Электронная почта",
        null=True,
        blank=True)
    delivery_address = models.TextField(
        null=True,
        blank=True)
    comments = models.TextField(
        "Комментарии к заказу",
        null=True,
        blank=True)
    created_date = models.DateTimeField(
        auto_now=False,
        auto_now_add=True)
    updated_date = models.DateTimeField(
        auto_now=True,
        auto_now_add=False)

    def __str__(self):
        return "Заказ № {}, от {}".format(
            self.pk,
            self.created_date.strftime('%d.%m.%Y'))

