from django.views.generic import FormView, CreateView, UpdateView, ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from .models import Authors, Series, Genre, Publisher, Producer
from .forms import AuthForm, SeriesForm, GenreForm, ProducerForm, PublisherForm
from admincore.views import HeadBar

class ShowAuthForm(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'html_form_ref.html'
    form_class = AuthForm
    model = Authors
    permission_required = "reference.add_authors"
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = "Добавление автора"
        context['dlist'] = HeadBar.create_bar(self.request)
        context.update(kwargs)
        return super().get_context_data(**context)
    success_url = "/reference/author/list/"

class ShowAuthFormUp(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'html_form_ref.html'
    form_class = AuthForm
    permission_required = "reference.change_authors"
    model = Authors
    def get_context_data(self, *args, **kwargs):
        #родительский метод
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Изменить данные автора'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['temp'] = self.kwargs  # справочник с id
        return context
    success_url = "/reference/author/list/"

class ShowAuthList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'html_form_list.html'
    model = Authors
    permission_required = "reference.view_authors"
    paginate_by = 15
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список авторов'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['temp'] = '/reference/author/'
        context['dir_address'] = '/reference/author/delete'
        return context

class DeleteSelectedAuthor(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'html_form_delete.html'
    model = Authors
    permission_required = "reference.delete_authors"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удаление автора'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url ="/reference/author/list/"

##############################################################
class ShowSeriesForm(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'html_form_ref.html'
    form_class = SeriesForm
    permission_required = "reference.add_series"
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = "Добавление серии"
        context['dlist'] = HeadBar.create_bar(self.request)
        context.update(kwargs)
        return super().get_context_data(**context)
    success_url ="/reference/series/list/"

class ShowSeriesFormUp(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'html_form_ref.html'
    form_class = SeriesForm
    model = Series
    permission_required = "reference.change_series"
    def get_context_data(self, *args, **kwargs):
        #родительский метод
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Изменить серию'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url ="/reference/series/list/"

class ShowSeriesList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'html_form_list.html'
    model = Series
    permission_required = "reference.view_series"
    paginate_by = 15
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список серий'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['temp'] = '/reference/series/'
        context['dir_address'] = '/reference/series/delete'
        return context

class DeleteSelectedSeries(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'html_form_delete.html'
    model = Series
    permission_required = "reference.delete_series"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удаление серии'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url ="/reference/series/list/"


##################################################

class ShowGenreForm(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'html_form_ref.html'
    form_class = GenreForm
    model = Genre
    permission_required = "reference.add_genre"
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = "Добавление жанра"
        context['dlist'] = HeadBar.create_bar(self.request)
        context.update(kwargs)
        return super().get_context_data(**context)
    success_url = "/reference/genre/list/"

class ShowGenreFormUp(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'html_form_ref.html'
    form_class = GenreForm
    model = Genre
    permission_required = "reference.change_genre"
    def get_context_data(self, *args, **kwargs):
        #родительский метод
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Изменить жанр'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/reference/genre/list/"

class ShowGenreList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'html_form_list.html'
    model = Genre
    permission_required = "reference.view_genre"
    paginate_by = 15
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список жанров'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['temp'] = '/reference/genre/'
        context['dir_address'] = '/reference/genre/delete'
        return context

class DeleteSelectedGenre(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'html_form_delete.html'
    model = Genre
    permission_required = "reference.delete_genre"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удаление жанра'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url ="/reference/genre/list/"

####################################################
class ShowPublisherForm(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'html_form_ref.html'
    form_class = PublisherForm
    permission_required = "reference.add_publisher"
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = "Добавление издателя"
        context['dlist'] = HeadBar.create_bar(self.request)
        context.update(kwargs)
        return super().get_context_data(**context)
    success_url = "/forms/publisher/list/"

class ShowPublisherFormUp(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'html_form_ref.html'
    form_class = PublisherForm
    model = Publisher
    permission_required = "reference.change_publisher"
    def get_context_data(self, *args, **kwargs):
        #родительский метод
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Изменить наименование издателя'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/reference/publisher/list/"

class ShowPublisherList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'html_form_list.html'
    model = Publisher
    permission_required = "reference.view_publisher"
    paginate_by = 15
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список издателей'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['temp'] = '/reference/publisher/'
        context['dir_address'] = '/reference/publisher/delete'
        return context


class DeleteSelectedPublisher(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'html_form_delete.html'
    model = Publisher
    permission_required = "reference.delete_publisher"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удаление издателя'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url ="/reference/publisher/list/"

############################################
class ShowProducerForm(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'html_form_ref.html'
    form_class = ProducerForm
    permission_required = "reference.add_producer"
    def get_context_data(self, **kwargs):
        context = {}
        context['title_'] = "Добавление производителя"
        context['dlist'] = HeadBar.create_bar(self.request)
        context.update(kwargs)
        return super().get_context_data(**context)
    success_url = "/reference/producer/list/"

class ShowProducerFormUp(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'html_form_ref.html'
    form_class = ProducerForm
    model = Producer
    permission_required = "reference.change_producer"
    def get_context_data(self, *args, **kwargs):
        #родительский метод
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Изменить наименование производителя'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/reference/producer/list/"

class ShowProducerList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'html_form_list.html'
    model = Producer
    permission_required = "reference.view_producer"
    paginate_by = 15
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список производителей'
        context['dlist'] = HeadBar.create_bar(self.request)
        context['temp'] = '/reference/producer/'
        context['dir_address'] = '/reference/producer/delete'
        return context

class DeleteSelectedProducer(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'html_form_delete.html'
    model = Producer
    permission_required = "reference.delete_producer"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удаление производителя'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/reference/producer/list/"
