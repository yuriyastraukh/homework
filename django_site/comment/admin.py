from django.contrib import admin
from .models import Comments


class Comments_admin(admin.ModelAdmin):
    list_display=[
        'comment_field',
        'content_type',
        'object_id',
        'created_date',
        'user_field',
    ]

    class Meta:
        model = Comments

admin.site.register(Comments, Comments_admin)
