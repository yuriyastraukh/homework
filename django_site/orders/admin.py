from django.contrib import admin
from .models import Order

class OrderAdmin(admin.ModelAdmin):
    list_display=[
        'cart',
        'status',
        'phone',
        'created_date',
        'updated_date',
    ]

    class Meta:
        model = Order

admin.site.register(Order, OrderAdmin)
