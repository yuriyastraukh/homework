from django.contrib import admin
from .models import Books



# admin.site.register(Books)



class BooksAdmin(admin.ModelAdmin):
    list_display=[
        'name',
        'year_p',
        'ISBN'
    ]

    class Meta:
        model = Books

admin.site.register(Books, BooksAdmin)