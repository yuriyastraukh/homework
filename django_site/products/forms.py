from django import forms
from .models import Books

class BooksForm(forms.ModelForm):
    class Meta:
        model = Books
        fields = [
            'name',
            'image_f',
            'price',
            'author_f',
            'seriesOfbook',
            'genre',
            'year_p',
            'nompages',
            'binding',
            'formatofbook',
            'ISBN',
            'weight',
            'age_rest',
            'publisher',
            'producer',
            'stockofbooks',
            'act_on_off',
            'rate_b'
        ]

        def clean(self):
            cleaned_data = super().clean()
            ISBN = cleaned_data.get("ISBN")
            if re.search('\D', ISBN):
                self.add_error('ISBN', 'is unfit')
                raise forms.ValidationError("mess")
            return cleaned_data