from django.contrib import admin

from .models import Authors, Series, Genre, Publisher, Producer, OrderStatus
from products.models import Books

admin.site.register(Authors)
admin.site.register(Series)
admin.site.register(Genre)
admin.site.register(Publisher)
admin.site.register(Producer)
admin.site.register(OrderStatus)