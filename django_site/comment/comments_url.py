from django.urls import path

from .views import AddComment

app_name = 'comment'

urlpatterns =[
    path('comments-add', AddComment.as_view(), name='comments-add'),
]

