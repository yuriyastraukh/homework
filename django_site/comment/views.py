from django.views.generic.base import RedirectView
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from .models import Comments
from django.contrib.auth.models import User, Group

CurrentUser = get_user_model()
class AddComment(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        comment = self.request.POST.get('comment')
        cont_t = self.request.POST.get('cont_t')
        obj_id = self.request.POST.get('obj_id')
        user_f = self.request.user
        next = self.request.POST.get('next', '/')
        if cont_t and obj_id and comment:
            cont_t_= ContentType.objects.get_for_id(cont_t)
            comment_=Comments.objects.create(
                comment_field = comment,
                user_field = user_f,
                content_type = cont_t_,
                object_id = obj_id,
            )
        return next

