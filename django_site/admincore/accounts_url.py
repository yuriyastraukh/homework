from django.urls import path
from .views import  LogInUser, LogOutUser

app_name = 'accounts'


urlpatterns = [
    path('login/', LogInUser.as_view(), name='log-in-user'),
    path('logout/', LogOutUser.as_view(), name='log-out-user'),
]