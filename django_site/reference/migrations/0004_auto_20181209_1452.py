# Generated by Django 2.1.4 on 2018-12-09 11:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reference', '0003_remove_authors_slug'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='series',
            options={'ordering': ('series',), 'verbose_name': 'Серия', 'verbose_name_plural': 'Серия книг'},
        ),
        migrations.RenameField(
            model_name='series',
            old_name='seriese',
            new_name='series',
        ),
    ]
