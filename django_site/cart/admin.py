from django.apps import AppConfig
from django.contrib import admin
from . import models

# class CartConfig(AppConfig):
#     name = 'cart'

class CartAdmin(admin.ModelAdmin):
    list_display=[
        'pk',
        'user',
        'created_date',
        'updated_date'
    ]

    class Meta:
        model = models.Cart

admin.site.register(models.Cart, CartAdmin)

class ProdAdmin(admin.ModelAdmin):
    list_display=[
        'cart',
        'book',
        'quantity'
        ]

    class Meta:
        model = models.Products

admin.site.register(models.Products, ProdAdmin)