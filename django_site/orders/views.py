from django.views.generic import CreateView, DetailView, ListView, UpdateView, DeleteView
from django.core.paginator import Paginator
from django.urls import reverse_lazy
from admincore.views import HeadBar
from .forms import CheckoutOrderForm, ChangeModelOrder
from .models import Order
from reference.models import OrderStatus


class OrderView(CreateView):
    model = Order
    template_name = 'cart/cart-list.html'
    form_class = CheckoutOrderForm

    def get_success_url(self):
        del self.request.session['cart_id']
        return reverse_lazy('orders:order-done', kwargs={'pk': self.object.pk})

class OrderDone(DetailView):
    model = Order
    template_name = 'order-done.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Завершение заказа'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context

class OrderList(ListView):
    model = Order
    template_name = 'orders-list.html'
    paginate_by = 10
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Лист заказов'
        context['count_orders'] = Order.objects.all().count()
        context['count_in_process'] = Order.objects.filter(status__pk=2).count()
        context['dlist'] = HeadBar.create_bar(self.request)
        return context

class ChangeOrder(UpdateView):
    model = Order
    template_name='change_book_in_cart.html'
    form_class = ChangeModelOrder


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Изменить заказ'
        object_st = context['object']
        object_st.status = OrderStatus.objects.get(pk='2')
        object_st.save()
        context['dlist'] = HeadBar.create_bar(self.request)

        return context
    success_url = reverse_lazy('orders:order-list')

class DeleteOrder(DeleteView):
    model = Order
    template_name = 'html_form_delete.html'
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удаление заказ'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = reverse_lazy('orders:order-list')