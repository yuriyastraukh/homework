from django import forms
from .models import Authors, Series, Genre, Publisher, Producer

class AuthForm(forms.ModelForm):
    class Meta:
        model = Authors
        fields = [
            'name'
        ]

class SeriesForm(forms.ModelForm):
    class Meta:
        model = Series
        fields = [
            'name'
        ]

class GenreForm(forms.ModelForm):
    class Meta:
        model = Genre
        fields = [
            'name'
        ]

class PublisherForm(forms.ModelForm):
    class Meta:
        model = Publisher
        fields = [
            'name'
        ]

class ProducerForm(forms.ModelForm):
    class Meta:
        model = Producer
        fields = [
            'name'
        ]