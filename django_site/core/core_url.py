from django.urls import path
from .views import UserView, Purchases
from cart.views import AddProductToCart

app_name = 'shopuser'


urlpatterns = [
    path(r'', UserView.as_view(), name='shopuser'),
    path('purchases/', Purchases.as_view(), name='book_purchases'),
    path('purchases/addtocart/<int:pk>', AddProductToCart.as_view(), name='book_add'),
]