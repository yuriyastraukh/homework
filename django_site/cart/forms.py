from django import forms
from .models import Products

class AddToCartForm(forms.ModelForm):

    class Meta:
        model = Products
        fields = [
            'book',
            'quantity',
        ]

        widgets = {
            'book': forms.HiddenInput()
        }

class ChangeBookInCart(forms.ModelForm):

    class Meta:
        model = Products
        fields = [
            'book',
            'quantity',
        ]
