from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.db import models

class Comments(models.Model):
    comment_field = models.TextField("comment_field")
    user_field = models.ForeignKey(User, verbose_name="Имя пользователя", null = True, blank = True, on_delete = models.CASCADE)
    created_date = models.DateTimeField(verbose_name="Дата создания комментария", auto_now=False, auto_now_add=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    def __str__(self):
        return self.comment_field