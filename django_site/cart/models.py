from django.db import models
from django.contrib.auth import get_user_model
from products.models import Books
from django.contrib.auth.models import User
CurrentUser = get_user_model()
class Cart(models.Model):
    user = models.ForeignKey(User, related_name = 'cart_of_purchase', null = True, blank = True, on_delete = models.CASCADE)
    created_date = models.DateTimeField(verbose_name = "Создано", auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(verbose_name = "Изменено", auto_now=True, auto_now_add=False)

    def __str__(self):
        return 'Корзина N{}'.format(
            self.pk
        )

    @property
    def products_in_cart_count(self):
        total = 0
        for product in self.product.all():
            total += product.quantity
        return total

    @property
    def total_price(self):
        total = 0
        for product in self.product.all():
            total += product.price_total
        return total

    class Meta:
        verbose_name = "Корзина",
        verbose_name_plural = "Корзины"

class Products(models.Model):
    cart = models.ForeignKey(Cart, related_name='product', on_delete=models.CASCADE)
    book = models.ForeignKey(Books, related_name='book_in_cart', on_delete = models.CASCADE)
    quantity = models.IntegerField('Количество', default = 0)

    def __str__(self):
        return "Book {name} in {cart_id}".format(
            name=self.book.pk, cart_id=self.cart.pk
        )
    @property
    def price(self):
        return self.book.price

    @property
    def price_total(self):
        return self.book.price * self.quantity

    class Meta:
        verbose_name = "Продукт"
        verbose_name_plural = "Продукты"
        unique_together = (
            ('cart', 'book'),
        )
