from django.db import models

class Authors(models.Model):
    name = models.CharField("Автор", max_length=200, db_index=True)

    class Meta:
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'
        ordering = ('name',)

    def __str__(self):
        return self.name

class Series(models.Model):
    name = models.CharField("Серия",max_length=200, db_index=True)
    see = models.Manager()
    class Meta:
        verbose_name = 'Серия'
        verbose_name_plural = 'Серия книг'
        ordering = ('name',)

    def __str__(self):
        return self.name

class Genre(models.Model):
    name = models.CharField('Жанр', max_length=200, db_index=True)
    gee = models.Manager()
    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'
        ordering = ('name',)

    def __str__(self):
        return self.name

class Publisher(models.Model):
    name = models.CharField('Издательство',max_length=300, db_index=True)

    class Meta:
        verbose_name = 'Издательство'
        verbose_name_plural = 'Издательство'
        ordering = ('name',)

    def __str__(self):
        return self.name

class Producer(models.Model):
    name = models.CharField('Типография',max_length=300, db_index=True)

    class Meta:
        verbose_name = 'Изготовитель'
        verbose_name_plural = 'Изготовитель'
        ordering = ('name',)

    def __str__(self):
        return self.name

class OrderStatus(models.Model):
    name = models.CharField('Статус заказа',max_length=300, db_index=True)

    class Meta:
        verbose_name = 'Статус заказа'
        verbose_name_plural = 'Статусы заказа'
        ordering = ('name',)

    def __str__(self):
        return self.name
