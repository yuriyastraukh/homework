from django.urls import path
from . import views

app_name = 'reference'

urlpatterns = [
    path('author/create/', views.ShowAuthForm.as_view(), name='showauthorform'),
    path('author/<int:pk>/', views.ShowAuthFormUp.as_view(), name='ShowAuthFormUp'),
    path('author/list/', views.ShowAuthList.as_view(), name='ShowAuthList'),
    path('author/delete/<int:pk>/', views.DeleteSelectedAuthor.as_view(), name='deleteselectedauthor'),
    path('series/create/', views.ShowSeriesForm.as_view(), name='ShowSeriesForm'),
    path('series/<int:pk>/', views.ShowSeriesFormUp.as_view(), name='ShowSeriesFormUp'),
    path('series/list/', views.ShowSeriesList.as_view(), name='ShowSeriesList'),
    path('series/delete/<int:pk>/', views.DeleteSelectedSeries.as_view(), name='DeleteSelectedSeries'),
    path('genre/create/', views.ShowGenreForm.as_view(), name='ShowGenreForm'),
    path('genre/<int:pk>/', views.ShowGenreFormUp.as_view(), name='ShowGenreFormUp'),
    path('genre/list/', views.ShowGenreList.as_view(), name='ShowGenreList'),
    path('genre/delete/<int:pk>/', views.DeleteSelectedGenre.as_view(), name='DeleteSelectedGenre'),
    path('publisher/create/', views.ShowPublisherForm.as_view(), name='ShowPublisherForm'),
    path('publisher/<int:pk>/', views.ShowPublisherFormUp.as_view(), name='ShowPublisherFormUp'),
    path('publisher/list/', views.ShowPublisherList.as_view(), name='ShowPublisherList'),
    path('publisher/delete/<int:pk>/', views.DeleteSelectedPublisher.as_view(), name='DeleteSelectedPublisher'),
    path('producer/create/', views.ShowProducerForm.as_view(), name='ShowProducerForm'),
    path('producer/<int:pk>/', views.ShowProducerFormUp.as_view(), name='ShowProducerFormUp'),
    path('producer/list/', views.ShowProducerList.as_view(), name='ShowProducerList'),
    path('producer/delete/<int:pk>/', views.DeleteSelectedProducer.as_view(), name='DeleteSelectedProducer'),
]