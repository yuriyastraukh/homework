from django.urls import path
from .views import ShowformBook, ShowUpformBook, ShowBooksList, DeleteSelectedBooks, ChangeBook

app_name = 'products'

urlpatterns = [
    path('create/', ShowformBook.as_view(), name='create_book'),
    path('<int:pk>/', ChangeBook.as_view(), name='change_book'),
    path('show/<int:pk>/', ShowUpformBook.as_view(), name='show_book'),
    path('list/', ShowBooksList.as_view(), name='list_of_books'),
    path('delete/<int:pk>/', DeleteSelectedBooks.as_view(), name='delete_book'),
]
