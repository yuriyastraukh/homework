from django import template
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from comment.models import Comments

CurrentUser = get_user_model()
register = template.Library()

@register.inclusion_tag('comment.html', takes_context=True)
def comment_tag(context, obj, next="/"):
    cont_t = ContentType.objects.get_for_model(obj)
    comment = Comments.objects.filter(
        content_type = cont_t,
        object_id=obj.id,
    )
    return {
        'cont_t': cont_t.pk,
        'obj_id': obj.pk,
        'comment': comment,
        'next': next,
    }