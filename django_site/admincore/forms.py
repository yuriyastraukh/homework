from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, label="Имя", required=False)
    last_name = forms.CharField(max_length=30, label="Фамилия", required=False)
    email = forms.EmailField(max_length=254, help_text='Обязательное поле.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )