from django.urls import path

from .views import OrderView, OrderDone, OrderList, ChangeOrder, DeleteOrder

app_name = "orders"

urlpatterns = [
    path('order-check/', OrderView.as_view(), name='order-check'),
    path('order-done/<int:pk>', OrderDone.as_view(), name='order-done'),
    path('list/', OrderList.as_view(), name='order-list'),
    path('change/<int:pk>', ChangeOrder.as_view(), name='orders-list-change'),
    path('delete/<int:pk>', DeleteOrder.as_view(), name='orders-list-delete')
]