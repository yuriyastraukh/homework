from django.views.generic import CreateView, UpdateView, ListView, DeleteView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from admincore.views import HeadBar
from .models import Books
from .forms import BooksForm

class ShowformBook(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'html_form_ref.html'
    form_class = BooksForm
    model = Books
    permission_required = "products.add_books"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Создать книгу'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/books/list"

class ChangeBook(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'html_form_ref.html'
    form_class = BooksForm
    model = Books
    permission_required = "products.change_books"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['temp'] = self.kwargs  # справочник с id
        context['title_'] = 'Изменить данные о книге'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = "/books/list"

class ShowUpformBook(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'html_form_det.html'
    form_class = BooksForm
    model = Books
    permission_required = "products.view_books"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['temp'] = self.kwargs  # справочник с id
        context['title_'] = 'Данные о книге {}'.format(context['books'])
        context['dlist'] = HeadBar.create_bar(self.request)
        return context


class ShowBooksList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'html_form_listbooks.html'
    model = Books
    paginate_by = 15
    permission_required = "products.view_books"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Список книг'
        context['temp'] = "/books/"
        context['dir_address'] = '/books/delete'
        context['show_address'] = '/books/show/'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = 'list_of_books'

class DeleteSelectedBooks(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'html_form_delete.html'
    model = Books
    permission_required = "products.delete_books"
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['title_'] = 'Удалить книгу'
        context['dlist'] = HeadBar.create_bar(self.request)
        return context
    success_url = '/shop-admin/user-list'





