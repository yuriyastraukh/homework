from django.urls import path, re_path
from .views import AdminView, AdminStatistics, UserInformation, LogInUser, LogOutUser, ChangePassword, \
    ResetPassword, ResetPasswordConfirm, ResetPasswordDone, ResetPasswordComplete, signup, UpdateUserData, \
    SaveUserInformation, ListOfUsers, DeleteUser, UpdateUserDataManager, SaveUserInformationByManager

app_name = 'shopadmin'


urlpatterns = [
    path(r'', AdminView.as_view(), name='shop_admin'),
    path('statistics/', AdminStatistics.as_view(), name='admin_statistics'),
    path('update-profile/', UpdateUserData.as_view(), name='update-profile'),
    path('change-user-data/', SaveUserInformation.as_view(), name='change-user-data'),
    path('user-informaion/', UserInformation.as_view(), name='user-information'),
    path('user-list/', ListOfUsers.as_view(), name='user-list'),
    path('change-user-manager/<int:pk>', UpdateUserDataManager.as_view(), name='change-user-manager'),
    path('change-user-save/', SaveUserInformationByManager.as_view(), name='change-user-save'),
    path('delete-user/<int:pk>', DeleteUser.as_view(), name='user-delete'),
    path('registration/', signup, name='registration'),
    # path('login/', LogInUser.as_view(), name='log-in-user'),
    # path('logout/', LogOutUser.as_view(), name='log-out-user'),
    path('change-password/', ChangePassword.as_view(), name='change-password'),
    path('password-reset/', ResetPassword.as_view(), name='reset-password'),
    path('password-reset/done/', ResetPasswordDone.as_view(), name='password-reset-done'),
    re_path(r'password-reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', ResetPasswordConfirm.as_view(), name = 'reset-password-confirm'),
    path('password-reset/complete/', ResetPasswordComplete.as_view(), name='password-reset-complete'),
]